# 实现的Mapper接口

#### UserMapper

1. getAllUsers 得到所有用户信息
2. addUser 注册
3. queryUser 得到个人信息
4. updateUser 修改个人信息
5. deleteUser 删除个人信息

#### GoodsMapper

1. addGoods
2. queryGoodsByKind 参数：类型
3. queryGoodsById 参数：商品id
4. updateGoods
5. deleteGoods

#### CollectMapper

1. getAllCollectIdByUserId
2. addCollect
3. deleteCollect

#### ReleaseMapper

1. getAllReleaseIdByUserId
2. addRelease
3. deleteRelease

#### OrdersMapper

1. getAllOrdersIdByBuyerId
2. getAllOrdersBySellerId
3. addOrder
4. deleteOrder

#### PicturesMapper

1. getAllPicturesByGoodsId
2. deletePicturesByGoodsId
3. updatePicturesByGoodsId
4. addPicturesByGoodsId









