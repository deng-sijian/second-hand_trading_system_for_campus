package com.xmu.project;

import com.xmu.project.bean.*;
import com.xmu.project.config.AllPath;
import com.xmu.project.config.Config;
import com.xmu.project.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xmu.project.mapper")
public class ApplicationTest {

    @Autowired
    private AccountService accountService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private CollectService collectService;
    @Autowired
    private ReleaseService releaseService;
    @Autowired
    private PictureService pictureService;
    @Test
    public void testAccountService()
    {
        try {
//            User user = new User("name", 1, "password", 1, new Date(), "123", "123");
//            User newUser = new User("ai", 1, "password", 1, new Date(), "123", "123");
//            accountService.removeUser(user.getId());
//            accountService.userRegister(user);
//            System.out.println(accountService.getAllUsers().toString());
//
//            accountService.userUpdate(newUser);
            User user = new User("ai",1,"password",null,null,null,null );
            System.out.println(accountService.userLogin(user));
        }catch (NullPointerException e)
        {
            System.out.println("Null pointer!");
        }
    }
    @Test
    public void testOrderService()
    {
//        System.out.println(orderService.addOrder(order));
//        System.out.println(orderService.addOrder(order1));
//        System.out.println(orderService.addOrder(order2));
        System.out.println(orderService.getAllOrders());

        System.out.println(orderService.getAllOrdersByBuyerId(1).toString());

        System.out.println(orderService.getAllOrdersBySellerId(1).toString());
    }
    @Test
    public void testCollectService()
    {
//        Collect collect = new Collect(1,1);
//        Collect collect1 = new Collect(1,2);
//        Collect collect2 = new Collect(2,1);

//        System.out.println(collectService.addCollect(collect));
//        System.out.println(collectService.addCollect(collect2));
//        System.out.println(collectService.addCollect(collect1));


//        System.out.println(collectService.getAllCollectById(1));
//
//        System.out.println(collectService.removeCollect(new Collect(1,1)));
    }
    @Test
    public void testReleaseService()
    {
        Release collect = new Release(1,1);
        Release collect1 = new Release(1,2);
        Release collect2 = new Release(2,1);

        System.out.println(releaseService.addRelease(collect));
        System.out.println(releaseService.addRelease(collect2));
        System.out.println(releaseService.addRelease(collect1));

        System.out.println(releaseService.getAllRelease(1));

        System.out.println(releaseService.removeRelease(collect));

    }
    @Test
    public void testPictureService()
    {
        Picture pic = new Picture(1,1,"temp","wow","awesome");
        Picture pic1 = new Picture(2,1,"temp","wow","awesome");
        Picture pic2 = new Picture(1,2,"temp","wow","awesome");
//        System.out.println(pictureService.addPictures(pic));
//        System.out.println(pictureService.deletePicturesByPictureId(1));
//        System.out.println(pictureService.addPictures((pic)));
//        System.out.println(pictureService.updatePictures(pic2));
        System.out.println(pictureService.getAllPicturesByGoodsId(2));
    }
    @Test
    public void testGoodsService()
    {

        //System.out.println(goodsService.insertGoods(goods));
        //System.out.println(goodsService.queryGoods(2));
//        System.out.println(goodsService.removeGoods(goods));
//
//        goodsService.insertGoods(goods);
//        goods.setDesc("nooooo");
        Goods goods = new Goods(3,"a",1,new Date(),BigDecimal.valueOf(1.1),"dsad","String","String");
         System.out.println(goodsService.updateGoods(goods));
//
       // System.out.println(goodsService.queryGoodsName("yes"));
//
        System.out.println(goodsService.queryGoodsType("yres"));

    }


    @Test
    public void testOs(){
        System.out.println(System.getProperties().getProperty("os.name").toLowerCase().contains("windows"));
        System.out.println(AllPath.path);
    }
}