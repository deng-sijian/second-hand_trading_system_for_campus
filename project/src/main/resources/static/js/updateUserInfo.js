$(document).ready(function ()
{
    $("#msgSubmit").click(function ()
    {
        var name=$("#inputName").val();
        if(name==null)
        {
            alert("昵称不能为空！");
            return;
        }
        var password1=$("#inputPassword1").val();
        var password2=$("#inputPassword2").val();
        if(password1==null)
        {
            alert("密码不能为空！");
            return;
        }
        if(password1!=password2)
        {
            alert("两次密码不同,请重新输入!");
            return;
        }
        var sex;
        var sexJudge=$("#inputSex").val();
        if(sexJudge=="男生") sex="1";
        else if(sexJudge=="女生") sex="2";
        else if(sexJudge=="保密") sex="0";
        var birthday=$("#inputBirthday").val();
        var phone=$("#inputPhone").val();
        var email=$("#inputEmail").val();
        var d={
            name:name,
            password:password1,
            sex:sex,
            birthday:birthday,
            phone:phone,
            email:email,
        }
        $.ajax({
            url:'/user/updateUserImg',
            contentType: false,
            processData:false,
            type:'POST',
            success:function (result){
                alert(result.message);
            }
        });
        $.ajax({
            url: '/user/updateUserInfo',
            contentType:'application/json',
            dataType: 'text',
            type: 'POST',
            cache:false,
            data: JSON.stringify(d),
            success:function (result) {
                let obj;
                if((typeof result=='object')&&result.constructor==Object){
                    obj=result;
                }else{
                    obj  = eval("("+result+")");
                }
                //判断是否是json格式
                if(obj.resultCode==200)
                {
                    alert("修改成功！");
                    window.location.href="/user/myCenter";
                }
                else
                {
                    alert("修改失败！");
                    alert(obj.message);
                }
            },
            error:function (xhr,exception)
            {
                alert(xhr.status);
            },
    });
    });
});