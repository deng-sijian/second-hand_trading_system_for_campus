$(document).ready(function () {
    $("#registerButton").click(function ()
    {
        var name=$("#name").val();
        var password=$("#password").val();
        var passwordCommit=$("#passwordCommit").val();
        var phoneNumber=$("#phone").val();
        if(name=="")
        {
            alert("昵称不能为空！");
            return;
        }
        if(password=="")
        {
            alert("密码不能为空！");
            return;
        }
        if(password!=passwordCommit)
        {
            alert("前后输入密码不相等！");
            return;
        }
        if(phoneNumber.length!=11)
        {
            alert("请输入正确的手机号！");
            return ;
        }
        //alert(phoneNumber);
        var d={name:name,password:password,phoneNumber:phoneNumber};
        $.ajax({
            url: '/user/register',
            contentType:'application/json',
            dataType: 'text',
            type: 'POST',
            cache:false,
            data: JSON.stringify(d),
            success:function (result) {
                let obj;
                if((typeof result=='object')&&result.constructor==Object){
                    obj=result;
                }else{
                    obj  = eval("("+result+")");
                }
                //判断是否是json格式
                if(obj.resultCode==200)
                {
                    alert("注册成功！id是"+obj.message);
                    window.location.href="/user/login";
                }
                else
                {
                    alert(obj.resultCode);
                    alert("错误！");
                }
            },
            error:function (xhr,exceptioon)
            {
                alert(xhr.status);
            },
        })
    });
});