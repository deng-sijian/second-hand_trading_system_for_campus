$(document).ready(function () {
    $.ajax({
        url: '/user/isLogin',
        dataType: 'text',
        type: 'GET',
        cache:false,
        success:function (result) {
            var obj;
            if((typeof result=='object')&&result.constructor==Object){
                obj=result;
            }else{
                obj  = eval("("+result+")");
            }
            if(obj.resultCode==200)
            {
                $("#loginLi").hide();
            }
            else if(obj.resultCode==500)
            {
                $("#centerLi").hide();
            }
        },
        error:function (xhr,exceptioon)
        {
            alert(xhr.status);
        },
    });
})