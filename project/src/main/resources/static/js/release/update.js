$(document).ready(function ()
{
    $(".imageUpdate").change(function (){
        alert("图片添加");
        var objUrl = getObjectURL(this.files[0]);
        $('.imagePreview').attr("src", objUrl);
        $('.imagePreview').css({opacity: 0});
        $('.imagePreview').show(0).animate({opacity:1},1000);
    });
    $(".secImageUpdate").change(function (){
        alert("次要图片添加");
        var files=this.files;
        var div=document.getElementById("secImagePreview");
        div.innerHTML="";
        for(var i=0;i<files.length;i++)
        {
            var objUrl=getObjectURL(files[i]);
            var img=document.createElement("img");
            img.src=objUrl;
            img.style.width="100px";
            div.appendChild(img);
        }
    })
    function getObjectURL(file) {
        var url = null ;
        if (window.createObjectURL!=undefined) { // basic
            url = window.createObjectURL(file) ;
        } else if (window.URL!=undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file) ;
        } else if (window.webkitURL!=undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file) ;
        }
        return url ;
    }
    $("#goodsUpdateButton").click(function ()
    {

        //alert("触发");
        var name=$("#inputName").val();
        if(name==null)
        {
            alert("商品名不能为空！");
            return;
        }
        var qty=$("#inputQty").val();
        if(qty==null)
        {
            alert("数量不能为空！")
            return;
        }
        var price=$("#inputPrice").val();
        if(price==null)
        {
            alert("价格不能为空！");
            return;
        }
        var s=location.href;
        s=s.split('/');
        s=s[s.length-1];
        id=s;
        //alert("id="+id);

        var desc=$("#inputDesc").val();
        var type=$("#inputType").val();
        var d={
            id:id,
            name:name,
            qty:qty,
            price:price,
            desc:desc,
            type:type,
        }
        var files=$(".imageUpdate").get(0).files[0];
        var formData=new FormData();
        formData.append("photo",files);

        var secfile=$(".secImageUpdate").get(0).files;

        $.ajax({
            url: '/goods/update',
            contentType:'application/json',
            dataType: 'text',
            type: 'POST',
            cache:false,
            data: JSON.stringify(d),
            success:function (result) {
                let obj;
                if((typeof result=='object')&&result.constructor==Object){
                    obj=result;
                }else{
                    obj  = eval("("+result+")");
                }
                //判断是否是json格式
                //alert("success");
                if(obj.resultCode==200)
                {
                    //alert(obj.message);
                    for(var i=0;i<secfile.length;i++)
                    {
                        var secFormData=new FormData();
                        secFormData.append("photo",secfile[i]);
                        $.ajax({
                            url:'/goods/secImgUpdate',
                            contentType: false,
                            processData:false,
                            data:secFormData,
                            type:'POST',
                            async:false,
                            success:function (result)
                            {
                                //alert("第"+i+"张图片上传成功！");
                            }
                        });
                    }
                    $.ajax({
                        url:'/goods/descImgUpdate',
                        contentType: false,
                        processData:false,
                        data:formData,
                        type:'POST',
                        success:function (result)
                        {
                            //alert(result.message);
                        }
                    });
                    window.location.href="/user/release/index";
                }
                else
                {
                    alert(obj.resultCode);
                    alert("错误！"+obj.message);
                }
                alert("修改成功！");
                window.location.href=obj.message;
            },
            error:function (xhr,exceptioon)
            {
                alert(xhr.status);
            },
        });



    })
})