$(document).ready(function () {
    $("#loginButton").click(function () {
        if ($("#id").val() == "") {
            alert("用户ID不能为空!");
            return;
        }
        if ($("#password").val() == "") {
            alert("密码不能为空!");
            return;
        }
        var d = {id: $("#id").val(), password: $("#password").val()};
        $.ajax({
            url: '/user/login',
            contentType: 'application/json',
            dataType: 'text',
            type: 'POST',
            cache: false,
            data: JSON.stringify(d),
            success: function (result) {
                let obj;
                if ((typeof result == 'object') && result.constructor == Object) {
                    obj = result;
                } else {
                    obj = eval("(" + result + ")");
                }
                //判断是否是json格式
                if (obj.resultCode == 200) {
                    // alert("登录成功！");
                    if ($("#id").val() == "1")
                        window.location.href = "/admin/index";
                    else
                        window.location.href = "/index";
                } else {
                    alert(result.message);
                }
            },
            error: function (xhr, exceptioon) {
                alert(xhr.status);
            },
        });
    });
});


