$(document).ready(function ()
{
    $("#updateSubmit").click(function ()
    {
        var name=$("#inputName").val();
        var id;
        var s=location.href;
        s=s.split('/');
        s=s[s.length-1];
        id=s;
        if(name==null)
        {
            alert("昵称不能为空！");
            return;
        }
        var password=$("#inputPassword").val();
        if(password==null)
        {
            alert("密码不能为空！");
            return;
        }
        var sex;
        var sexJudge=$("#inputSex").val();
        if(sexJudge=="男生") sex="1";
        else if(sexJudge=="女生") sex="2";
        else if(sexJudge=="保密") sex="0";
        var birthday=$("#inputBirthday").val();
        var phone=$("#inputPhone").val();
        var email=$("#inputEmail").val();
        var d={
            id:id,
            name:name,
            password:password,
            sex:sex,
            birthday:birthday,
            phone:phone,
            email:email,
        }
        $.ajax({
            url: '/admin/userManagement/updateUser',
            contentType:'application/json',
            dataType: 'text',
            type: 'POST',
            cache:false,
            data: JSON.stringify(d),
            success:function (result) {
                let obj;
                if((typeof result=='object')&&result.constructor==Object){
                    obj=result;
                }else{
                    obj  = eval("("+result+")");
                }
                //判断是否是json格式
                if(obj.resultCode==200)
                {
                    alert("修改成功！");
                    window.location.href="/admin/index";
                }
                else
                {
                    alert(obj.message);
                }
            },
            error:function (xhr,exception)
            {
                alert(xhr.status);
                alert(exception);
            },
        });
    });
});