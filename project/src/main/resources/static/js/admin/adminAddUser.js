$(document).ready(function () {
    $("#userSubmit").click(function ()
    {
        var name=$("#inputName").val();
        var password=$("#inputPassword").val();
        var sex;
        var sexJudge=$("#inputSex").val();
        if(sexJudge=="男生") sex="1";
        else if(sexJudge=="女生") sex="2";
        else if(sexJudge=="保密") sex="0";
        var birthday=$("#inputBirthday").val();
        var phone=$("#inputPhone").val();
        var email=$("#inputEmail").val();
        if(name=="")
        {
            alert("昵称不能为空！");
            return;
        }
        if(password=="")
        {
            alert("密码不能为空！");
            return;
        }
        let d = {name: name, password: password, sex: sex, birthday: birthday, phone: phone, email: email};
        $.ajax({
            url: '/user/register',
            contentType:'application/json',
            dataType: 'text',
            type: 'POST',
            cache:false,
            data: JSON.stringify(d),
            success:function (result) {
                let obj;
                if((typeof result=='object')&&result.constructor==Object){
                    obj=result;
                }else{
                    obj  = eval("("+result+")");
                }
                //判断是否是json格式
                if(obj.resultCode==200)
                {
                    alert("注册成功！id是"+obj.message);
                    window.location.href="/admin";
                }
                else
                {
                    alert("错误！");
                }
            },
            error:function (xhr,exceptioon)
            {
                alert(xhr.status);
            },
        })
    });
});