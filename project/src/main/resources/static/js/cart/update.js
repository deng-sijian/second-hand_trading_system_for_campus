$(document).ready(
    function (){
        $('.inputQuantity').change(function () {
            var quantity = $(this).val();
            var goodsId = $(this).parent("div").find(":first").text();
            var unitPrice=$(this).parent("div").parent("div").parent("div").children("p.1").children("span").text();
            var totalPrice=unitPrice*quantity;
            $(this).parent("div").parent("div").parent("div").children("p.2").children("span").text(totalPrice);
            let d ={quantity:quantity,goodsId:goodsId};
            $.ajax(
                {
                    url: '/cart/update',
                    contentType:'application/json',
                    dataType: 'text',
                    type: 'POST',
                    cache:false,
                    data: JSON.stringify(d),
                    success:function (result) {
                        let obj;
                        if((typeof result=='object')&&result.constructor==Object){
                            obj=result;
                        }else{
                            obj  = eval("("+result+")");
                        }
                        //判断是否是json格式
                        if(obj.resultCode!=200)
                        {
                            alert(obj.message);
                        }
                    },
                    error:function (xhr,exceptioon)
                    {
                        alert(xhr.status);
                    },
                }
            )
        })
    }

// $('input').live('input propertychange', function()
// {
//     //获取input 元素,并实时监听用户输入
//     //逻辑
// })
)