$(document).ready(
    function (){
        $("#wantBuyButton").click(
            function (){
                var id = $("#goodsId").text();
                var quantity = $("#inputQuantity").val();

                let d={goodsId:id,quantity:quantity};
                $.ajax({
                    url:'/cart/collect',
                    contentType:'application/json',
                    dataType: 'text',
                    type: 'POST',
                    cache:false,
                    data: JSON.stringify(d),
                    success:function (result) {
                        let obj;
                        if((typeof result=='object')&&result.constructor==Object){
                            obj=result;
                        }else{
                            obj  = eval("("+result+")");
                        }
                        //判断是否是json格式
                        if(obj.resultCode==200)
                        {
                            alert("添加购物车成功");
                        }
                        else
                        {
                            alert(obj.message);
                        }
                    },
                    error:function (xhr,exceptioon)
                    {
                        alert(xhr.status);
                    },
                })
            }
        )
    }
)