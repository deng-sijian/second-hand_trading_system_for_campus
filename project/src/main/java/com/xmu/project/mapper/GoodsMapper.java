package com.xmu.project.mapper;

import com.xmu.project.bean.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GoodsMapper {
    //得到所有商品
    List<Goods> getAllGoods();
    //添加商品
    Integer addGoods(Goods goods);
    //根据类型查询商品
    List<Goods> queryGoodsByKind(String kind);
    //根据id查询商品
    Goods queryGoodsById(Integer id);
    //根据名称查询商品
    List<Goods> queryGoodsByName(String name);
    //更新商品信息
    Integer updateGoods(Goods newUser);
    //删除商品
    Integer deleteGoods(Integer id);
}
