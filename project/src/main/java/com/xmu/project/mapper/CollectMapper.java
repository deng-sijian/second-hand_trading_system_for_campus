package com.xmu.project.mapper;

import com.xmu.project.bean.Collect;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CollectMapper {
    //根据用户id获得所有的收藏
    List<Collect> getAllCollectByUserId(Integer userid);
    //
    Integer addCollect(Collect collect);
    //
    Integer deleteCollect(Collect collect);
    //
    Integer updateCollect(Collect collect);
}
