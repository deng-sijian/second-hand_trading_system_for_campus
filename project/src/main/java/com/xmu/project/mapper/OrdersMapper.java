package com.xmu.project.mapper;

import com.xmu.project.bean.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrdersMapper {
    //根据买家id来得到订单
    List<Order> getAllOrdersIdByBuyerId(Integer id);
    //根据卖家id来得到订单
    List<Order> getAllOrdersIdBySellerId(Integer id);
    //添加订单
    Integer addOrder(Order order);
    //得到所有订单
    List<Order> getAllOrders();
    //根据订单号来查询订单
    Order queryOrder(Integer id);
    //删除订单
    Integer deleteOrder(Integer id);
}
