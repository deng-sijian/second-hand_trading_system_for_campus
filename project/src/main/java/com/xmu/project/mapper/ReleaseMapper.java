package com.xmu.project.mapper;

import com.xmu.project.bean.Release;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ReleaseMapper {
    //根据用户id得到所有的Release
    List<Release> getAllReleaseIdByUserId(Integer id);
    //添加Release
    Integer addRelease(Release release);
    //删除Release
    Integer deleteRelease(Release release);
    //通过商品得到发布
    Release getReleaseByGoodsId(Integer id);
}
