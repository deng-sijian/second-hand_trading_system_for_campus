package com.xmu.project.mapper;

import com.xmu.project.bean.Picture;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PicturesMapper {
    //根据商品id找到所有图片
    List<Picture> getAllPicturesByGoodsId(Integer id);
    //删除
    Integer deletePicturesByPictureId(Integer id);
    //更新
    Integer updatePictures(Picture picture);
    //添加图片
    Integer addPictures(Picture picture);
}
