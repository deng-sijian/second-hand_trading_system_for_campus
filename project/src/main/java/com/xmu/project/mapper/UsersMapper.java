package com.xmu.project.mapper;

import com.xmu.project.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UsersMapper {
    //得到所有的用户
    List<User> getAllUsers();
    //添加用户
    Integer addUser(User user);
    //得到个人信息
    User queryUser(Integer id);
    //修改个人信息
    Integer updateUser(User newUser);
    //删除用户
    Integer deleteUser(Integer id);
}
