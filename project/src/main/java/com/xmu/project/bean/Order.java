package com.xmu.project.bean;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    private Integer goods_id;
    private Integer seller_id;
    private Integer buyer_id;
    private String goods_name;
    private BigDecimal goods_price;
    private String goods_desc;
    private String goods_type;
    private Date order_datetime;
    private Integer id;
    private Integer goods_quantity;
}
