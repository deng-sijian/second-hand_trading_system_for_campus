package com.xmu.project.bean;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Release {
    private Integer goods_id;
    private Integer user_id;
}
