package com.xmu.project.bean;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String name;
    private Integer id;
    private String password;
    private Integer sex;
    private Date birthday;
    private String phone;
    private String email;
}
