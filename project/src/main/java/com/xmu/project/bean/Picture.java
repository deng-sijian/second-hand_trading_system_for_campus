package com.xmu.project.bean;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Picture {
    private Integer pic_id;
    private Integer goods_id;
    private String image;
    private String name;
    private String desc;
}
