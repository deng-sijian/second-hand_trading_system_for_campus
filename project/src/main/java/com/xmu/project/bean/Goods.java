package com.xmu.project.bean;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Goods {
    private Integer id;
    private String name;
    private Integer quantity;
    private Date release_time;
    private BigDecimal price;
    private String desc;
    private String type;
    private String desc_image;
}
