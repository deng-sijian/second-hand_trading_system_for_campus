package com.xmu.project.bean;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Collect {
    private Integer goods_id;
    private Integer user_id;
    private Integer quantity;
}
