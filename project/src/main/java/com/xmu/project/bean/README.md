# 实体类的存放目录

这里采用了lombok的插件的简便写法，不用直接写也行，或者直接delombok获得原始代码
例如：
```java
import lombok.*;

@NoArgsConstructor//无参构造方法
@FullArgsConstructor//全参构造方法
@Data
public class Template{
    private String id;
    private String name;
}
```