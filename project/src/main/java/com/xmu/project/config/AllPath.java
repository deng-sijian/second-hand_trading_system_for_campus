package com.xmu.project.config;

public class AllPath {


    // TODO　图片存放的路径
    public static final String WINDOWS_GOODS_IMG_PATH="E:\\xmu_shop_pic\\";

    public static final String LINUX_GOODS_IMG_PATH="/usr/games/xmu_shop_pic/";


    public static final String LINUX_SEPARATOR = "/";

    public static final String WINDOWS_SEPARATOR = "\\";

    public static String path;

    public static String separator;

    static{
        String systemName = System.getProperties().getProperty("os.name").toLowerCase();

        if(systemName.contains("windows")) { //windows 启用
            path = WINDOWS_GOODS_IMG_PATH;
            separator = WINDOWS_SEPARATOR;
        }
            //linux 启用
        else {
            path = LINUX_GOODS_IMG_PATH;
            separator = LINUX_SEPARATOR;
        };

    }

  //  public static String path="/usr/games/xmu_shop_pic";
    public static Integer editingGoodsId;

    public static void setEditingGoodsId(Integer editingGoodsId) {
        AllPath.editingGoodsId = editingGoodsId;
    }
}
