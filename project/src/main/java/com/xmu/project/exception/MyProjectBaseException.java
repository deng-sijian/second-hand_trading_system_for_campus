package com.xmu.project.exception;

public class MyProjectBaseException extends Exception{
    public MyProjectBaseException() {
    }
    public MyProjectBaseException(String message) {
        super(message);
    }
    public MyProjectBaseException(String message, Throwable cause) {
        super(message, cause);
    }
    public MyProjectBaseException(Throwable cause) {
        super(cause);
    }
    public MyProjectBaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
