package com.xmu.project.vo;

import com.xmu.project.bean.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CartGoods{
    private Integer totalPrice;
    private String desc_img;
    private Integer goods_id;
    private Integer seller_id;
    private Integer buyer_id;
    private String goods_name;
    private BigDecimal goods_price;
    private String goods_desc;
    private String goods_type;
    private Date order_datetime;
    private Integer id;
    private Integer goods_quantity;
    public CartGoods(Order order)
    {
        this.desc_img=null;
        goods_id=order.getGoods_id();
        seller_id=order.getSeller_id();
        buyer_id=order.getBuyer_id();
        goods_name=order.getGoods_name();
        goods_price=order.getGoods_price();
        goods_desc=order.getGoods_desc();
        goods_type=order.getGoods_type();
        order_datetime=order.getOrder_datetime();
        id=order.getId();
        goods_quantity=order.getGoods_quantity();
    }
}
