package com.xmu.project.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminGoods {
    Integer goodsId;
    Integer sellerId;
    String goodsName;
    String sellerName;
    String type;
    Integer quantity;
    Date releaseTime;
    String descImage;
}
