package com.xmu.project.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItem {
    private Integer goodsId;
    private String name;
    private Integer quantity;
    private Integer stock;
    private BigDecimal unitPrice;
    private BigDecimal totalPrice;
    private String descImage;
    private Integer status;
}
