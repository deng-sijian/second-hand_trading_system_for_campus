package com.xmu.project.vo;


import com.xmu.project.bean.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOrderItem {
    private Integer id;
    private String name;
    private List<Order> orderList;
}
