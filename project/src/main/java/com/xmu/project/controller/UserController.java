package com.xmu.project.controller;

import com.xmu.project.bean.User;
import com.xmu.project.service.AccountService;
import com.xmu.project.util.RandomKeyGenerator;
import com.xmu.project.util.Result;
import com.xmu.project.util.ResultGenerator;
import com.xmu.project.util.StringVerifyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.GenericFilter;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")

public class UserController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "login",method = RequestMethod.GET)
    public String getLogin(HttpSession httpSession) {
        if(httpSession.getAttribute("loginUserId")!=null)
        {
            return "index";
        }
        return "user/login";
    }
    @RequestMapping(value = "login",method = RequestMethod.POST)
    @ResponseBody
    public Result login(@RequestBody Map<String,String> map,
                        HttpSession httpSession)
    {
        User user;
        System.out.println("id:"+map.get("id"));
        String ide=String.valueOf(map.get("id"));
        String password=map.get("password");
        System.out.println("length"+ide.length());
        if(ide.length()<=10)
        {
            Integer id=Integer.valueOf(ide);
            System.out.println("id:"+id+"\n"+"password："+password);
            user=new User(null,id,password,null,null,null,null);
            user=accountService.userLogin(user);
            if(user==null)
            {
                System.out.println("账号或密码错误！");
                return ResultGenerator.genFailResult("账号或密码错误！");
            }
            else{
                if(user.getId()==1)
                    httpSession.setAttribute("role","admin");
                else httpSession.setAttribute("role","user");
                httpSession.setAttribute("loginUserName",user.getName());
                httpSession.setAttribute("loginUserId",user.getId());
                httpSession.setAttribute("loginUserPassword",user.getPassword());
                System.out.println("登陆成功！");
                return ResultGenerator.genSuccessResult("登陆成功！");
            }
        }
        else if(ide.length()==11)
        {
            List <User> users=accountService.getAllUsers();
            for(User u:users)
            {
                if(u.getPhone()==null) continue;
                if(u.getPhone().equals(ide))
                {
                    if(u.getPassword().equals(password))
                    {
                        httpSession.setAttribute("loginUserName",u.getName());
                        httpSession.setAttribute("loginUserId",u.getId());
                        httpSession.setAttribute("loginUserPassword",u.getPassword());
                        System.out.println("登陆成功！");
                        return ResultGenerator.genSuccessResult("登陆成功！");
                    }
                    System.out.println("手机号：账号或密码错误");
                    return ResultGenerator.genFailResult("账号或密码错误！");
                }

            }
        }

        System.out.println("账号长度错误！");
        return ResultGenerator.genFailResult("账号长度错误！");

    }

    /*登录操作函数*/
    @GetMapping("/register")
    public String register()
    {
        return "user/register";
    }

    @PostMapping("/register")
    @ResponseBody
    public Result register(
            @RequestBody Map<String,Object> map) throws ParseException {
        String name=(String)map.get("name");
        String password=(String)map.get("password");
        String phoneNumber=(String)map.get("phoneNumber");
        System.out.println(map.get("phoneNumber"));
        Integer id= RandomKeyGenerator.generateIntKey(10);
        while(!ObjectUtils.isEmpty(accountService.queryUser(id)))
        {
            id= RandomKeyGenerator.generateIntKey(10);
        }
        User user=new User(name,id,password,null,null,phoneNumber,null);
        System.out.println(user);
        accountService.userRegister(user);
        return ResultGenerator.genSuccessResult(String.valueOf(id));
    }
    /*注册页面,注册成功后返回id;注意：浏览器传入的id随意，返回值为id*/
    @RequestMapping(value="/myCenter")
    public String myCenter(HttpSession httpSession,Model model)
    {
        Integer id=(Integer)httpSession.getAttribute("loginUserId");
        if(id==null) return "error/error_404";
        List<User> users=accountService.getAllUsers();
        User user=new User();
        if(httpSession.getAttribute("loginUserId")==null) return "user/login";
        for(User u:users) {
            if (u.getId().equals(id))
            {
                user = u;
                break;
            }
        }
        model.addAttribute("name",user.getName());
        model.addAttribute("id",user.getId());
        model.addAttribute("password",user.getPassword());
        model.addAttribute("sex",user.getSex());
        model.addAttribute("birthday",user.getBirthday());
        model.addAttribute("phone",user.getPhone());
        model.addAttribute("email",user.getEmail());
        return "user/myCenter";
    }
    @RequestMapping(value="/myCenter/{id}")
    public String myCenter(@PathVariable("id") String ide, HttpSession httpSession,Model model)
    {
        Integer id=Integer.valueOf(ide);
        List<User> users=accountService.getAllUsers();
        User user=new User();
        if(httpSession.getAttribute("loginUserId")==null) return "user/login";
        for(User u:users) {
            if (u.getId().equals(id))
            {
                user = u;
                break;
            }
        }
        model.addAttribute("name",user.getName());
        model.addAttribute("id",user.getId());
        model.addAttribute("password",user.getPassword());
        model.addAttribute("sex",user.getSex());
        model.addAttribute("birthday",user.getBirthday());
        model.addAttribute("phone",user.getPhone());
        model.addAttribute("email",user.getEmail());
        return "user/center";
    }
    /*个人中心*/
    @RequestMapping(value="updateUserInfo",method=RequestMethod.GET)
    public String updateInfo(HttpSession httpSession, Model model)
    {
        Integer id=(Integer) httpSession.getAttribute("loginUserId");
        List<User> users= accountService.getAllUsers();
        User user=new User();
        for(User u:users)
        {
            if(u.getId().equals(id))
            {
                user=u;
            }
        }
        model.addAttribute("name",user.getName());
        model.addAttribute("id",user.getId());
        model.addAttribute("password",user.getPassword());
        model.addAttribute("sex",user.getSex());
        model.addAttribute("birthday",user.getBirthday());
        model.addAttribute("phone",user.getPhone());
        model.addAttribute("email",user.getEmail());
        return "user/update";
    }

    @RequestMapping(value="updateUserImg",method = RequestMethod.POST)
    @ResponseBody
    public Result editImg(HttpSession httpSession,
                          @RequestParam MultipartFile photo)
    {
        if(photo.isEmpty())
        {
            System.out.println("上传图片为空！");
            return ResultGenerator.genFailResult("接收失败！图片为空！");
        }
        System.out.println(photo.getOriginalFilename());
        return ResultGenerator.genSuccessResult("接收成功");
    }

    @RequestMapping(value = "updateUserInfo",method=RequestMethod.POST)
    @ResponseBody
    public Result editInfo(HttpSession httpSession,
                           @RequestBody Map<String,Object> map)
    {
        try {
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            String name=(String) map.get("name");
            String password=(String) map.get("password");
            Integer sex;
            if((String)map.get("sex")==null) sex=null;
            else sex=Integer.valueOf((String)map.get("sex"));
            Date birthday;
            if((String)map.get("birthday")==null) birthday=null;
            else birthday=simpleDateFormat.parse((String)map.get("birthday"));
            String phone=(String) map.get("phone");
            if(phone.length()!=11) return ResultGenerator.genFailResult("电话格式错误！");
            String email=(String) map.get("email");
            if(!StringVerifyUtil.isEmail(email)) return ResultGenerator.genFailResult("邮箱格式错误！");
            Integer id=(Integer)httpSession.getAttribute("loginUserId");
            User user=new User(name,id,password,sex,birthday,phone,email);
            accountService.userUpdate(user);
            httpSession.setAttribute("loginUserName",name);
            httpSession.setAttribute("loginUserPassword",password);
            return ResultGenerator.genSuccessResult("修改成功");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }
    /*用户信息修改*/
    @RequestMapping(value="logout")
    public String logout(HttpSession httpSession)
    {
        httpSession.setAttribute("loginUserName",null);
        httpSession.setAttribute("loginUserId",null);
        httpSession.setAttribute("loginUserPassword",null);
        return "index";
    }

    @RequestMapping(value="isLogin",method = RequestMethod.GET)
    @ResponseBody
    public Result isLogin(HttpSession httpSession)
    {
        if(httpSession.getAttribute("loginUserId")!=null) return ResultGenerator.genSuccessResult("登录");
        else return ResultGenerator.genFailResult("未登录");
    }
}
