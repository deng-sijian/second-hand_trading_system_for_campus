package com.xmu.project.controller;

import com.xmu.project.bean.Goods;
import com.xmu.project.bean.Order;
import com.xmu.project.bean.Release;
import com.xmu.project.bean.User;
import com.xmu.project.service.AccountService;
import com.xmu.project.service.GoodsService;
import com.xmu.project.service.OrderService;
import com.xmu.project.service.ReleaseService;
import com.xmu.project.util.Result;
import com.xmu.project.util.ResultGenerator;
import com.xmu.project.util.StringBlurMatch;
import com.xmu.project.util.StringVerifyUtil;
import com.xmu.project.vo.AdminGoods;
import com.xmu.project.vo.UserOrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author deng-sijian
 * 管理员控制器类
 * 21/7/9
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private ReleaseService releaseService;

    @RequestMapping(value ={"", "/index"},method = RequestMethod.GET)
    public String index(Model model,
                        HttpSession httpSession){
        httpSession.setAttribute("currentPath","/admin/index");
        model.addAttribute("userList",accountService.getAllUsers());
        model.addAttribute("orderIdList",orderService.getAllOrders());
        model.addAttribute("orderUserList",accountService.getAllUsers());
        List<Goods> allGoods = goodsService.getAllGoods();
        List<AdminGoods> goodsList =genAdminGoodsList(allGoods);
        model.addAttribute("goodsList",goodsList);
        return "admin/index";
    }

    @RequestMapping(value = "/userManagement/addUser",method = RequestMethod.GET)
    public String addUser(HttpServletRequest request){
        return "admin/userManagement/addUser";
    }

    @RequestMapping(value = "/userManagement/getAllUsers",method = RequestMethod.GET)
    public Result getAllUsers(){
        return ResultGenerator.genSuccessResult(accountService.getAllUsers());
    }

    @RequestMapping(value = "/userManagement/removeUser",method = RequestMethod.GET)
    public String removeUser(@RequestParam("id") Integer id,Model model){
        List<Release> allRelease = releaseService.getAllRelease(id);
        for(Release release:allRelease) {
            goodsService.removeGoods(new Goods(release.getGoods_id(),null,null,null,null,null,null,null));
        }
        accountService.removeUser(id);
        model.addAttribute("userList",accountService.getAllUsers());
        return "admin/index";
    }

    @RequestMapping(value = "/userManagement/updateUser",method=RequestMethod.POST)
    @ResponseBody
    public Result editInfo(@RequestBody Map<String,Object> map)
    {
        try {
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            String name=(String) map.get("name");
            String password=(String) map.get("password");
            Integer sex;
            if((String)map.get("sex")=="") sex=null;
            else sex=Integer.valueOf((String)map.get("sex"));
            Date birthday;
            if((String)map.get("birthday")==null) birthday=null;
            else birthday=simpleDateFormat.parse((String)map.get("birthday"));
            String phone=(String) map.get("phone");
            if(phone.length()!=11) return ResultGenerator.genFailResult("电话格式错误！");
            String email=(String) map.get("email");
            if(!StringVerifyUtil.isEmail(email)) return ResultGenerator.genFailResult("邮箱格式错误！");
            Integer id=Integer.parseInt((String)map.get("id"));
            User user=new User(name,id,password,sex,birthday,phone,email);
            accountService.userUpdate(user);
            return ResultGenerator.genSuccessResult("修改成功");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @RequestMapping(value = "/userManagement/updateUser/{id}",method = RequestMethod.GET)
    public String updateUser(@PathVariable("id") Integer id,
                             HttpServletRequest request,
                             Model model){
        User user;
        if(Objects.isNull(user=accountService.queryUser(id))){
            request.setAttribute("errorMsg","找不到用户");
            return "error/error_5xx";
        }
        else model.addAttribute("user",user);
        return "admin/userManagement/updateUser";
    }
    @RequestMapping(value = "/userManagement/search",method = RequestMethod.GET)
    public String getSearchUser(@RequestParam("keyWord") String keyWord,
                                Model model){
        if(keyWord==null)return "error/error_404";
        else if(keyWord.isBlank())return "admin/index";

        List<User> allUsers = accountService.getAllUsers();
        List<User> userList = allUsers.stream().filter(user -> {
            if (StringBlurMatch.StringBlurMatch2(user.getName(), keyWord) ||
                    StringBlurMatch.StringBlurMatch2(user.getId().toString(), keyWord))
                return true;
            else return false;
        }).collect(Collectors.toList());
        model.addAttribute("userList",userList);

        model.addAttribute("orderUserList",accountService.getAllUsers());
        model.addAttribute("orderIdList",orderService.getAllOrders());
        List<Goods> allGoods = goodsService.getAllGoods();
        List<AdminGoods> goodsList =genAdminGoodsList(allGoods);
        model.addAttribute("goodsList",goodsList);
        return "admin/index";
    }
    @RequestMapping(value = "/orderManagement/search",method = RequestMethod.GET)
    public String getIdSearch(@RequestParam("keyWord") String keyWord,
                              Model model){
        if(Objects.isNull(keyWord))return "error/error_404";
        if(keyWord.isBlank())return "redirect:user/myOrders";
        if(StringVerifyUtil.isInteger(keyWord)) {
            Integer userId = Integer.parseInt(keyWord);
            List<Order> allOrders = orderService.getAllOrdersByBuyerId(userId);
            allOrders.addAll(orderService.getAllOrdersBySellerId(userId));
            model.addAttribute("orderIdList",allOrders);
        }

        List<User> userList = accountService.getAllUsers()
                .stream()
                .filter(user -> StringBlurMatch.StringBlurMatch(user.getName(),keyWord))
                .collect(Collectors.toList());


        model.addAttribute("orderUserList",userList);
        model.addAttribute("userList",accountService.getAllUsers());
        List<Goods> allGoods = goodsService.getAllGoods();
        List<AdminGoods> goodsList =genAdminGoodsList(allGoods);
        model.addAttribute("goodsList",goodsList);
        return "admin/index";
    }

    @RequestMapping(value = "/orderManagement/orders/{id}",method = RequestMethod.GET)
    public String getOrders(@PathVariable("id") Integer id,
                            Model model,
                            HttpSession httpSession){
        httpSession.setAttribute("currentPath","/admin/orderManagement/orders/"+id);
        List<Order> allBargain = orderService.getAllOrdersByBuyerId(id);
        List<Order> allSoldOut = orderService.getAllOrdersBySellerId(id);

        model.addAttribute("user",accountService.queryUser(id));
        model.addAttribute("allBargain",allBargain);
        model.addAttribute("allSoldOut",allSoldOut);
        return "admin/orderManagement/orders";
    }

    @RequestMapping(value = "/goodsManagement/remove",method = RequestMethod.GET)
    public String getRemoveGoods(@RequestParam("id") Integer id){
       if(goodsService.removeGoods(new Goods(
                    id,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null))!=0){
                return "admin/index";
            }
            else return "error/error_5xx";
    }

    @RequestMapping(value = "/goodsManagement/search",method = RequestMethod.GET)
    public String getGoodsSearch(@RequestParam("keyWord") String keyWord,Model model){
        if(Objects.isNull(keyWord))return "error/error_404";
        if(keyWord.isBlank())return "redirect:user/myOrders";

        List<Goods> allGoods = goodsService.getAllGoods();

        List<Goods> collect = allGoods.stream().filter(goods ->
        {
            if (StringBlurMatch.StringBlurMatch(goods.getId().toString(), keyWord) ||
                    StringBlurMatch.StringBlurMatch(goods.getName(), keyWord) ||
                    StringBlurMatch.StringBlurMatch(goods.getType(), keyWord) ||
                    StringBlurMatch.StringBlurMatch(goods.getDesc(), keyWord)) return true;
            else return false;
        }).collect(Collectors.toList());

        List<AdminGoods> goodsList = genAdminGoodsList(collect);
        model.addAttribute("goodsList",goodsList);

        model.addAttribute("userList",accountService.getAllUsers());
        model.addAttribute("orderIdList",orderService.getAllOrders());
        model.addAttribute("orderUserList",accountService.getAllUsers());
        return "admin/index";

    }

    private List<AdminGoods> genAdminGoodsList(List<Goods> allGoods){
        List<AdminGoods> goodsList = new ArrayList<>();
        for(Goods goods:allGoods){
            goodsList.add(new AdminGoods(goods.getId(),
                    releaseService.getReleaseByGoodsId(goods.getId()).getUser_id(),
                    goods.getName(),
                    accountService.queryUser(releaseService.getReleaseByGoodsId(goods.getId()).getUser_id()).getName(),
                    goods.getType(),
                    goods.getQuantity(),
                    goods.getRelease_time(),
                    goods.getDesc_image()));
        }
        return goodsList;
    };

//    @RequestMapping(value = "removeOrder",method = RequestMethod.POST)
//    @ResponseBody
//    public Result removeOrder(@RequestBody Order order){
//    }
}
