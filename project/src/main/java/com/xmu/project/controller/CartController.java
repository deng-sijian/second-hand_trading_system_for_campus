package com.xmu.project.controller;

import com.xmu.project.bean.Collect;
import com.xmu.project.bean.Goods;
import com.xmu.project.common.CartItemEnum;
import com.xmu.project.service.CollectService;
import com.xmu.project.service.GoodsService;
import com.xmu.project.util.Result;
import com.xmu.project.util.ResultGenerator;
import com.xmu.project.util.StringBlurMatch;
import com.xmu.project.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author deng_sijian
 * @since 21/7/19
 */
@Controller
public class CartController {

    @Autowired
    private CollectService collectService;

    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = "/user/cart",method = RequestMethod.GET)
    public String getCart(Model model,
                          HttpSession session)
    {
        List<Collect> collects=collectService.getAllCollectById(
                (Integer)session.getAttribute("loginUserId"));
        List<CartItem> cartItems = genCartItems(collects);
        model.addAttribute("cartList",cartItems);
        return "user/cart";
    }

    @RequestMapping(value = "/cart/delete",method = RequestMethod.GET)
    public String getUpdate(@RequestParam("id") Integer id,
                            HttpSession httpSession){

        Integer userId =(Integer) httpSession.getAttribute("loginUserId");
        collectService.removeCollect(new Collect(id,userId,null));
        return "redirect:user/cart";
    }

    @RequestMapping(value = "/cart/purchase",method = RequestMethod.GET)
    public String purchase(@RequestParam("id") Integer id,
                           @RequestParam("quantity")Integer quantity,
                           HttpSession httpSession){

        Integer userId =(Integer) httpSession.getAttribute("loginUserId");
        collectService.removeCollect(new Collect(id,userId,null));
        String URL = "/goods/purchase?id="+id+"&quantity="+quantity;
        return "redirect:"+URL;
    }

    @RequestMapping(value = "/cart/collect",method = RequestMethod.POST)
    @ResponseBody
    public Result collect(@RequestBody HashMap<String,Object> map,
                          HttpSession httpSession){
        Integer userId = (Integer) httpSession.getAttribute("loginUserId");
        Integer quantity = Integer.parseInt((String) map.get("quantity"));
        Integer goodsId = Integer.parseInt((String) map.get("goodsId")) ;
        Collect collect = new Collect(goodsId,userId,quantity);
        collectService.addCollect(collect);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping(value = "/cart/update",method = RequestMethod.POST)
    @ResponseBody
    public Result update(@RequestBody HashMap<String,Object> map,
                         HttpSession httpSession){
        Integer quantity = Optional.of(Integer.parseInt((String) map.get("quantity"))).orElse(1);
        Integer goods_id = Optional.of(Integer.parseInt((String)map.get("goodsId"))).get();
        Integer user_id = (Integer) httpSession.getAttribute("loginUserId");
        if(Objects.isNull(collectService.updateCollect(new Collect(goods_id, user_id, quantity))))
            return ResultGenerator.genFailResult("参数异常");
        return ResultGenerator.genSuccessResult("更新成功");
    }

    @RequestMapping(value = "/cart/search",method = RequestMethod.GET)
    public String getSearch(@RequestParam("keyWord") String keyWord,
                            HttpSession httpSession,
                            Model model){
        Integer userId =(Integer) httpSession.getAttribute("loginUserId");
        List<Collect> collectList = collectService.getAllCollectById(userId)
                .stream()
                .filter(collect -> {
                    Goods goods = goodsService.queryGoods(collect.getGoods_id());
                    String desc = goods.getDesc();
                    String name = goods.getName();
                    if (StringBlurMatch.StringBlurMatch(desc, keyWord) ||
                            StringBlurMatch.StringBlurMatch(name, keyWord))
                        return true;
                    else return false;
                })
                .collect(Collectors.toList());

        List<CartItem> cartItems =genCartItems(collectList);
        model.addAttribute("cartList",cartItems);
        return "user/cart";
    }

    private List<CartItem> genCartItems(List<Collect> collects){
        List<CartItem> cartItems =new ArrayList<>();
        for(Collect c:collects) {
            Goods goods=goodsService.queryGoods(c.getGoods_id());
            //如果status的值等于0的话说明库存不足，收藏过期，等待补货再能选择
            if(goods.getQuantity()<c.getQuantity())
                cartItems.add(new CartItem(goods.getId(),
                        goods.getName(),
                        c.getQuantity(),
                        goods.getQuantity(),
                        goods.getPrice(),
                        goods.getPrice().multiply(BigDecimal.valueOf(c.getQuantity())),
                        goods.getDesc_image(),
                        CartItemEnum.OUT_OF_STORAGE.getType()));
                //如果status是1的话说明收藏还没有过期
            else
                cartItems.add(new CartItem(goods.getId(),
                        goods.getName(),
                    c.getQuantity(),
                        goods.getQuantity(),
                        goods.getPrice(),
                    goods.getPrice().multiply(BigDecimal.valueOf(c.getQuantity())),
                    goods.getDesc_image(),
                    CartItemEnum.ON_SALE.getType()));
        }
        return cartItems;
    }
}
