package com.xmu.project.controller;


import com.xmu.project.bean.Order;
import com.xmu.project.service.AccountService;
import com.xmu.project.service.GoodsService;
import com.xmu.project.service.OrderService;
import com.xmu.project.service.ReleaseService;
import com.xmu.project.util.StringBlurMatch;
import com.xmu.project.vo.CartGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = "/user/myOrders",method = RequestMethod.GET)
    public String getOrderIndex(Model model,
                                HttpSession httpSession){
        httpSession.setAttribute("currentPath","/user/myOrders");
        //我买的订单部分
        Integer userId = (Integer) httpSession.getAttribute("loginUserId");
        List<Order> myBargain = orderService.getAllOrdersByBuyerId(userId);

        //我卖出的订单部分
        List<Order> mySoldOut = orderService.getAllOrdersBySellerId(userId);
        List<CartGoods> bargain=new ArrayList<CartGoods>();
        for(Order order:myBargain)
        {
            CartGoods bar=new CartGoods(order);
            bar.setDesc_img(goodsService.queryGoods(bar.getGoods_id()).getDesc_image());
            bar.setTotalPrice(bar.getGoods_quantity()* bar.getGoods_price().intValue());
            bargain.add(bar);
        }
        List<CartGoods> soldOut=new ArrayList<CartGoods>();
        for(Order order:mySoldOut)
        {
            CartGoods sold=new CartGoods(order);
            sold.setDesc_img(goodsService.queryGoods(sold.getGoods_id()).getDesc_image());
            sold.setTotalPrice(sold.getGoods_quantity()*sold.getGoods_price().intValue());
            soldOut.add(sold);
        }
        model.addAttribute("myBargain",bargain);
        model.addAttribute("mySoldOut",soldOut);

        return "user/myOrders";
    }

    @RequestMapping(value = "/order/orderDetail/{id}",method = RequestMethod.GET)
    public String getOrderDetail(@PathVariable("id") Integer id,
                                 Model model) {
        Order order=orderService.getOrder(id);
        if (Objects.isNull(order)) return "error/error_404";
        else {
            model.addAttribute("orderDetail",order);
            Integer totalPrice=order.getGoods_price().intValue();
            model.addAttribute("totalPrice", totalPrice);
            String img=goodsService.queryGoods(order.getGoods_id()).getDesc_image();
            model.addAttribute("descImg",img);
            return "order/orderDetail";
        }
    }
    @RequestMapping(value = "/order/delete",method = RequestMethod.GET)
    public String getUpdate(@RequestParam("id") Integer id,
                         HttpSession httpSession){
        if(Objects.isNull(id))
            return "error/error_404";
        String path = (String) httpSession.getAttribute("currentPath");
        orderService.removeOrder(id);

        return "redirect:"+path;
    }

    @RequestMapping(value = "/order/search",method = RequestMethod.GET)
    public String getNameSearch(@RequestParam("keyWord") String keyWord,
                                HttpSession httpSession,
                                Model model){
        Integer userId = (Integer) httpSession.getAttribute("loginUserId");
        if(Objects.isNull(keyWord))return "error/error_404";
        if(keyWord.isBlank())return "redirect:user/myOrders";
        List<Order> allOrders = orderService.getAllOrders();
        List<Order> searchOrderList = allOrders.stream().filter(
                (order -> {
            if (StringBlurMatch.StringBlurMatch(order.getGoods_name(), keyWord)||
                    StringBlurMatch.StringBlurMatch(order.getGoods_desc(),keyWord)) return true;
            else return false;
        })).collect(Collectors.toList());

        List<Order> myBargain = searchOrderList
                .stream()
                .filter(order -> order.getBuyer_id().equals(userId))
                .collect(Collectors.toList());
        List<Order> mySoldOut = searchOrderList
                .stream()
                .filter((order -> order.getSeller_id().equals(userId)))
                .collect(Collectors.toList());
        List<CartGoods> bargain=new ArrayList<CartGoods>();
        for(Order order:myBargain)
        {
            CartGoods bar=new CartGoods(order);
            bar.setDesc_img(goodsService.queryGoods(bar.getGoods_id()).getDesc_image());
            bar.setTotalPrice(bar.getGoods_quantity()* bar.getGoods_price().intValue());
            bargain.add(bar);
        }
        List<CartGoods> soldOut=new ArrayList<CartGoods>();
        for(Order order:mySoldOut)
        {
            CartGoods sold=new CartGoods(order);
            sold.setDesc_img(goodsService.queryGoods(sold.getGoods_id()).getDesc_image());
            sold.setTotalPrice(sold.getGoods_quantity()*sold.getGoods_price().intValue());
            soldOut.add(sold);
        }
        model.addAttribute("myBargain",bargain);
        model.addAttribute("mySoldOut",soldOut);
        return "user/myOrders";
    }


}
