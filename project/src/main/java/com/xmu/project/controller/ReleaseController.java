package com.xmu.project.controller;

import com.xmu.project.bean.Goods;
import com.xmu.project.bean.Release;
import com.xmu.project.bean.User;
import com.xmu.project.service.AccountService;
import com.xmu.project.service.GoodsService;
import com.xmu.project.service.ReleaseService;
import com.xmu.project.util.Result;
import com.xmu.project.util.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/release")
public class ReleaseController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private ReleaseService releaseService;
    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = {"/","/index"},method = RequestMethod.GET)
    public String releaseIndex(HttpSession session,Model model){
        Integer id=(Integer)session.getAttribute("loginUserId");
        List<User> users=accountService.getAllUsers();
        User user=new User();
        for(User u:users)
        {
            if(u.getId().equals(id))
            {
                user=u;
                break;
            }
        }
        List<Integer> idList=new ArrayList<Integer>();
        for(Release r:releaseService.getAllRelease(id))
        {
            idList.add(r.getGoods_id());
        }
        List<Goods> list=new ArrayList<Goods>();
        for(Integer i:idList)
        {
            list.add(goodsService.queryGoods(i));
        }
        model.addAttribute("goodsList",list);
        return "user/release/index";
    }

    @RequestMapping(value = "release", method = RequestMethod.POST)
    public Result getReleaseIndex(HttpSession session, Model model)
    {
        List<Release> listRelease=releaseService.getAllRelease((Integer)session.getAttribute("loginUserId"));
        List<Goods> listGoods= new ArrayList<Goods>();
        for(Release r:listRelease)
        {
            listGoods.add(goodsService.queryGoods(r.getGoods_id()));
        }
        model.addAttribute("releaseGoodInfo",listGoods);
        return ResultGenerator.genSuccessResult();
    }
/**
 * 已经不需要了，因为其实本质上就是改商品，参见:
 * GoodsController.update(...)
 * GoodsController.remove(...)
 */
//    @RequestMapping(value = "release/edit")
//    public String updateRelease(HttpSession session,Model model,
//                          @RequestParam("id") Integer id,
//                          @RequestParam("name") String name,
//                          @RequestParam("quantity") Integer quantity,
//                          @RequestParam("release_time") Date release_time,
//                          @RequestParam("price") BigDecimal price,
//                          @RequestParam("desc") String desc,
//                          @RequestParam("type") String type,
//                          @RequestParam("desc_image") String desc_image
//    )
//    {
//        goods.updateGoods(new Goods(id,name,quantity,release_time,price,desc,type,desc_image));
//        return "redirect:/user/release";
//    }
//    @RequestMapping(value = "release/delete",method = RequestMethod.POST)
//    public Result deleteRelease(@RequestParam("id") Integer id) {
//        Goods goods=goodsService.queryGoods(id);
//        goodsService.removeGoods(goods);
//        return ResultGenerator.genSuccessResult();
//    }
}
