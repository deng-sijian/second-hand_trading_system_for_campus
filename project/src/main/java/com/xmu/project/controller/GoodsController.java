package com.xmu.project.controller;


import com.xmu.project.bean.Goods;
import com.xmu.project.bean.Order;
import com.xmu.project.bean.Picture;
import com.xmu.project.bean.Release;
import com.xmu.project.config.AllPath;
import com.xmu.project.service.GoodsService;
import com.xmu.project.service.OrderService;
import com.xmu.project.service.PictureService;
import com.xmu.project.service.ReleaseService;
import com.xmu.project.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author deng-sijian
 * 商品控制类
 */
@Controller
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ReleaseService releaseService;

    @Autowired
    private PictureService pictureService;

   @RequestMapping(value = {"/","/index"},method = RequestMethod.GET)
    public String getGoodsIndex(HttpSession session, HttpServletRequest request, Model model)
    {

        List<Goods> goodsList=goodsService.getAllGoods();
        System.out.println(goodsList);
        String path0=null;
        String path1=null;
        String path2=null;
        System.out.println(path0);
        System.out.println("size"+goodsList.size());
        if(goodsList.size()>=1)
        {
            path0=goodsList.get(0).getDesc_image();
            if(goodsList.size()>=2)
            {
                path1=goodsList.get(1).getDesc_image();
                if(goodsList.size()>=3)
                {
                    path2=goodsList.get(2).getDesc_image();
                }
            }
        }

        model.addAttribute("path0",path0);
        model.addAttribute("path1",path1);
        model.addAttribute("path2",path2);
        request.setAttribute("goodsList",goodsService.getAllGoods());
        model.addAttribute("goodsList",goodsService.getAllGoods());
        model.addAttribute("id",session.getAttribute("loginUserId"));
        return "index";
    }

   @RequestMapping(value = "/goods/getAllGoods",method = RequestMethod.POST)
   @ResponseBody
   public Result goodsIndex(Model model) {
       model.addAttribute("goodsList",goodsService.getAllGoods());
       return ResultGenerator.genSuccessResult();
   }

   @RequestMapping(value = "/goods/goodsDetail/{id}",method = RequestMethod.GET)
    public String goodsDetail(@PathVariable("id") Integer id,
                              Model model)
   {
       Goods goods;
       if(ObjectUtils.isEmpty(goods=goodsService.queryGoods(id))){
           model.addAttribute("errorMsg",ResultGenerator.genFailResult("商品不存在"));
           return "index";
       }
       else{
           model.addAttribute("goodsDetail",goods);
           List<Picture> pictures=pictureService.getAllPicturesByGoodsId(id);
           model.addAttribute("pictures",pictures);
           System.out.println(pictures);
           return "goods/goodsDetail";
       }
   }

   @RequestMapping(value = "/goods/purchase",method = RequestMethod.GET)
   public String purchase(@RequestParam("id") Integer id,
                          @RequestParam("quantity") Integer quantity,
                          HttpSession httpSession,
                          Model model)
   {
       Integer userId;
       Integer sellerId;
       Goods goods=goodsService.queryGoods(id);
       if(ObjectUtils.isEmpty(userId=(Integer)httpSession.getAttribute("loginUserId"))){
           httpSession.setAttribute("errorMsg","请先登录");
           return "error/error_404";
       }
       else if(ObjectUtils.isEmpty(goods)) {
           httpSession.setAttribute("errorMsg","商品不存在");
           return "error/error_404";
       }
       else if(goods.getQuantity()-quantity<0) {
           httpSession.setAttribute("errorMsg","商品数量不足");
           return "error/error_404";
       }
      else{
          Integer randomKey = RandomKeyGenerator.generateIntKey(10);
          while(orderService.getOrder(randomKey)!=null)randomKey =RandomKeyGenerator.generateIntKey(10);
          sellerId = releaseService.getReleaseByGoodsId(goods.getId()).getUser_id();
           Order order=new Order(
                   goods.getId(),
                   sellerId,
                   userId,
                   goods.getName(),
                   goods.getPrice().multiply(BigDecimal.valueOf(quantity)),
                   goods.getDesc(),
                   goods.getType(),
                   new Date(),
                   randomKey,
                   quantity);
          orderService.addOrder(order);
          goods.setQuantity(goods.getQuantity()-quantity);
          goodsService.updateGoods(goods);
          model.addAttribute("goodsDetail",goods);
          System.out.println(goods);
          model.addAttribute("orderDetail",order);
           System.out.println(order);
          return "common/getPurchase";
   }
   }

    @RequestMapping(value ="/goods/update/{id}",method =RequestMethod.GET)
    public String update(HttpSession session ,Model model,@PathVariable String id)
    {
        Goods goods=goodsService.queryGoods(Integer.valueOf(id));
        model.addAttribute("id",goods.getId());
        model.addAttribute("name",goods.getName());
        model.addAttribute("qty",goods.getQuantity());
        model.addAttribute("release_time",goods.getRelease_time());
        model.addAttribute("price",goods.getPrice());
        model.addAttribute("desc",goods.getDesc());
        model.addAttribute("type",goods.getType());
        model.addAttribute("desc_image",goods.getDesc_image());
        return "goods/update";
    }


    //linux启用
    @RequestMapping(value="/goods/secImgUpdate",method = RequestMethod.POST)
    @ResponseBody
    public Result secImgUpdate(@RequestParam(value = "photo") MultipartFile photo)
    {
        System.out.println("接受到副图片");
        if(photo.isEmpty()) return ResultGenerator.genFailResult("图片为空！");
        else
        {
            Integer id= AllPath.editingGoodsId;
            String path= AllPath.path+String.valueOf(id);
            String fileName=photo.getOriginalFilename();
            String suffixName=fileName.substring(fileName.lastIndexOf("."));
            String picId=RandomKeyGenerator.generateIntKey(10)+"";
            fileName=picId+suffixName;
            System.out.println(fileName);
            System.out.println("type::" + suffixName);
            System.out.println("filename::" + fileName);
            File targetDir=new File(path);
            if(!targetDir.exists())
            {
                targetDir.mkdirs();
            }
            String rootPath="/"+"static"+"/"+ id +"/"+fileName;
            System.out.println("rootPath:"+rootPath);
            File saveFile=new File(targetDir,fileName);
            try
            {
                photo.transferTo(saveFile);
                Picture picture=new Picture(Integer.valueOf(picId),id,rootPath,null,null);
                System.out.println("picture:"+picture);
                pictureService.addPictures(picture);
                System.out.println("rootPath:"+rootPath);
                return ResultGenerator.genSuccessResult("上传成功!");
            }
            catch (Exception e)
            {
                System.out.println("出错！"+e.getMessage());
                return ResultGenerator.genFailResult("上传失败！");
            }
        }


    }

    @RequestMapping(value="/goods/descImgUpdate",method = RequestMethod.POST)
    @ResponseBody
    public Result descImgUpdate(@RequestParam MultipartFile photo)
    {
        if(photo.isEmpty()) return ResultGenerator.genFailResult("图片为空！");
        else
        {
            Integer id= AllPath.editingGoodsId;
            System.out.println("id"+id);
            String path= AllPath.path+String.valueOf(id);
            String fileName=photo.getOriginalFilename();
            String suffixName=fileName.substring(fileName.lastIndexOf("."));
            String picId=RandomKeyGenerator.generateIntKey(10)+"";
            fileName= picId+suffixName;
            System.out.println(fileName);
            System.out.println("type::" + suffixName);
            System.out.println("filename::" + fileName);
            File targetDir=new File(path);
            if(!targetDir.exists())
            {
                targetDir.mkdirs();
            }
            String rootPath="/static/"+String.valueOf(id)+"/"+fileName;
            File saveFile=new File(targetDir,fileName);
            try
            {
                photo.transferTo(saveFile);
                Goods goods=goodsService.queryGoods(id);

                File fileDelete=new File(AllPath.path+goods.getDesc_image());
                if(fileDelete.exists())
                {
                    fileDelete.delete();
                }
                else System.out.println(fileDelete.getPath()+"是错误的路径！");
                goods.setDesc_image(rootPath);
                goodsService.updateGoods(goods);
                System.out.println("rootPath:"+rootPath);
                System.out.println("saveFile"+saveFile.getPath());
                return ResultGenerator.genSuccessResult("上传成功！");
            }
            catch (Exception e)
            {
                return ResultGenerator.genFailResult("上传失败！");
            }
        }
    }

    @RequestMapping(value = "/goods/update", method = RequestMethod.POST)
    @ResponseBody
    public Result update(@RequestBody Map<String,Object> map,
                         HttpSession httpSession) {
       Integer id=Integer.valueOf((String)map.get("id"));
       String name = (String) map.get("name");
       Integer qty = Integer.valueOf((String) map.get("qty"));
       BigDecimal price=new BigDecimal((String) map.get("price"));
       String desc=(String) map.get("desc");
       String type=(String) map.get("type");
       Date date=goodsService.queryGoods(id).getRelease_time();
       Goods goods=new Goods(id,name,qty,date,price,desc,type,null);
       goodsService.updateGoods(goods);
       AllPath.setEditingGoodsId(id);
       List<Picture> pictures=pictureService.getAllPicturesByGoodsId(id);
       for(Picture picture:pictures)
       {
           File file=new File(AllPath.path+picture.getImage());
           if(file.exists())
           {
               file.delete();
           }
           pictureService.deletePicturesByPictureId(picture.getPic_id());
       }
       if(httpSession.getAttribute("role").equals("user"))
       return ResultGenerator.genSuccessResult("/user/release/index");
       else return ResultGenerator.genSuccessResult("/admin/index");
    }
    /*@RequestMapping(value="/goods/imgUpdate",method = RequestMethod.POST)
    @ResponseBody
    public Result imgUpdate(@RequestParam MultipartFile[] photo)
    {

    }*/
    @RequestMapping(value="/goods/secImgUpload",method = RequestMethod.POST)
    @ResponseBody
    public Result secImgUpload(@RequestParam(value = "photo") MultipartFile photo)
    {
        System.out.println("接受到副图片");
        if(photo.isEmpty()) return ResultGenerator.genFailResult("图片为空！");
        else
        {
            Integer id= AllPath.editingGoodsId;
            String path= AllPath.path+String.valueOf(id);
            String fileName=photo.getOriginalFilename();
            String suffixName=fileName.substring(fileName.lastIndexOf("."));
            String picId=RandomKeyGenerator.generateIntKey(10)+"";
            fileName=picId+suffixName;
            System.out.println(fileName);
            System.out.println("type::" + suffixName);
            System.out.println("filename::" + fileName);
            File targetDir=new File(path);
            if(!targetDir.exists())
            {
                targetDir.mkdirs();
            }
            String rootPath="/"+"static"+"/"+String.valueOf(id)+"/"+fileName;
            System.out.println("rootPath:"+rootPath);
            File saveFile=new File(targetDir,fileName);
            try
            {
                photo.transferTo(saveFile);
                Picture picture=new Picture(Integer.valueOf(picId),id,rootPath,null,null);
                System.out.println("picture:"+picture);
                pictureService.addPictures(picture);
                System.out.println("副图片rootPath:"+rootPath);
                return ResultGenerator.genSuccessResult("上传成功!");
            }
            catch (Exception e)
            {
                System.out.println("出错！"+e.getMessage());
                return ResultGenerator.genFailResult("上传失败！");
            }
        }


    }

    @RequestMapping(value="/goods/descImgUpload",method = RequestMethod.POST)
    @ResponseBody
    public Result descImgUpload(@RequestParam MultipartFile photo)
    {
        if(photo.isEmpty()) return ResultGenerator.genFailResult("图片为空！");
        else
        {
            Integer id= AllPath.editingGoodsId;
            String path= AllPath.path+String.valueOf(id);
            String fileName=photo.getOriginalFilename();
            String suffixName=fileName.substring(fileName.lastIndexOf("."));
            fileName= RandomKeyGenerator.generateIntKey(10)+ suffixName;
            System.out.println(fileName);
            System.out.println("type::" + suffixName);
            System.out.println("filename::" + fileName);
            File targetDir=new File(path);
            if(!targetDir.exists())
            {
                targetDir.mkdirs();
            }
            String rootPath="/static/"+String.valueOf(id)+"/"+fileName;
            File saveFile=new File(targetDir,fileName);
            try
            {
                photo.transferTo(saveFile);
                Goods goods=goodsService.queryGoods(id);
                goods.setDesc_image(rootPath);
                goodsService.updateGoods(goods);
                System.out.println("主图片rootPath:"+rootPath);
                return ResultGenerator.genSuccessResult("上传成功！");
            }
            catch (Exception e)
            {
                return ResultGenerator.genFailResult("上传失败！");
            }
        }
    }

    @RequestMapping(value = "/goods/upload",method = RequestMethod.GET)
    public String getUpload(){
       return "goods/upload";
    }

    @RequestMapping(value = "/goods/upload",method = RequestMethod.POST)
    @ResponseBody
    public Result upload(@RequestBody Map<String,Object> map,
                         HttpSession httpSession)
    {
        System.out.println(map.toString());
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String name = (String) map.get("name");
            Integer qty = Integer.valueOf((String) map.get("qty"));
            Date release_time = simpleDateFormat.parse((String) map.get("date"));
            BigDecimal price=new BigDecimal((String) map.get("price"));
            String desc=(String) map.get("desc");
            String type=(String) map.get("type");
            Integer id;
            do
            {
                id= RandomKeyGenerator.generateIntKey(10);
            }
            while (!Objects.isNull(goodsService.queryGoods(id)));
            Goods goods = new Goods(id,name,qty,release_time,price,desc,type,null);
            goodsService.insertGoods(goods);
            releaseService.addRelease(new Release(id, (Integer) httpSession.getAttribute("loginUserId")));
            AllPath.setEditingGoodsId(id);
            return ResultGenerator.genSuccessResult(String.valueOf(id));
        }
        catch (Exception e)
        {
            System.out.println("错误："+e);
            return ResultGenerator.genFailResult(e.getMessage());
        }
    }

    @RequestMapping(value = "/goods/search",method = RequestMethod.GET)
    public String getSearch(@RequestParam("keyWord") String keyWord,
                            HttpServletRequest request,
                            Model model){
        if(StringUtils.hasLength(keyWord)){
            model.addAttribute("searchList",goodsService.queryGoodsName(keyWord));
        }
        else request.setAttribute("errorMsg","关键词不能为空");
       return "goods/search";
   }

    @RequestMapping(value = "/goods/search",method = RequestMethod.POST)
    @ResponseBody
    public Result search(@RequestParam("keyWord") String keyWord){
        if(StringUtils.hasLength(keyWord)){
            List<Goods> goodsList = goodsService.getAllGoods().stream()
                    .filter(goods -> {
                        if (StringBlurMatch.StringBlurMatch(goods.getName(), keyWord) ||
                                StringBlurMatch.StringBlurMatch(goods.getDesc(), keyWord))
                            return true;
                        else return false;
                    }).collect(Collectors.toList());
            return ResultGenerator.genSuccessResult(goodsList);
        }
        else return ResultGenerator.genFailResult("关键词不能为空");
    }

    @RequestMapping(value = "/goods/type",method = RequestMethod.GET)
    public String getSearchByType(@RequestParam("keyWord") String keyWord,
                                  Model model,
                                  HttpServletRequest request){
        if(StringUtils.hasLength(keyWord)){
            model.addAttribute("searchList",goodsService.queryGoodsType(keyWord));
            return "goods/search";
        }
        else request.setAttribute("errorMsg","关键字不能为空");
        return request.getContextPath();
    }

    @RequestMapping(value = "/goods/type",method = RequestMethod.POST)
    @ResponseBody
    public Result searchByType(@RequestParam("keyWord") String keyWord){
        if(StringUtils.hasLength(keyWord)){
            return ResultGenerator.genSuccessResult(goodsService.queryGoodsType(keyWord));
        }
        else return ResultGenerator.genFailResult("关键词不能为空");
    }

    @RequestMapping(value ="/goods/remove",method = RequestMethod.GET)
    public String remove(@RequestParam("id") Integer goodsId,
                         HttpSession httpSession,
                         Model model){

        Integer userId=(Integer)httpSession.getAttribute("loginUserId");
        Integer goodsOwnerId = releaseService.getReleaseByGoodsId(goodsId).getUser_id();
        if(!userId.equals(goodsOwnerId))return "error/error_5xx";
        else if(goodsService.removeGoods(new Goods(
                goodsId,
                null,
                null,
                null,
                null,
                null,
                null,
                null))!=0){
            List<Release> allRelease = releaseService.getAllRelease(userId);
            List<Goods> goods = new ArrayList<>();
            for(Release r:allRelease)
            {
                goods.add(goodsService.queryGoods(r.getGoods_id()));
            }
            model.addAttribute("goodList",goods);
            return "redirect:/user/release/index";
        }
        else return "error/error_5xx";


    }
}
