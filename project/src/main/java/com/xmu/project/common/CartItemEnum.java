package com.xmu.project.common;

import com.xmu.project.vo.CartItem;

public enum CartItemEnum {
    OUT_OF_STORAGE(0,"库存不足"),
    ON_SALE(1,"正常");

    int type;
    String name;
    CartItemEnum(int type,String name){
        this.type= type;
        this.name = name;
    }

    public static CartItemEnum getCartItemByType(int type) {
        for (CartItemEnum cartItemEnum : CartItemEnum.values()) {
            if (cartItemEnum.getType() == type) {
                return cartItemEnum;
            }
        }
        return ON_SALE;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }
}
