package com.xmu.project.service.Impl;

import com.xmu.project.bean.Collect;
import com.xmu.project.mapper.CollectMapper;
import com.xmu.project.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class CollectImpl implements CollectService {

    @Autowired
    private CollectMapper collectMapper;

    //根据用户id得到所有的收藏
    @Override
    public List<Collect> getAllCollectById(Integer id) { return collectMapper.getAllCollectByUserId(id); }

    //删除收藏
    @Override
    public Integer removeCollect(Collect collect) {
        return collectMapper.deleteCollect(collect);
    }


    //添加收藏
    @Override
    public Integer addCollect(Collect collect) {
        return collectMapper.addCollect(collect);
    }

    @Override
    public Integer updateCollect(Collect collect) {
        return collectMapper.updateCollect(collect);
    }
}
