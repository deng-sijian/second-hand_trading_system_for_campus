package com.xmu.project.service.Impl;

import com.xmu.project.bean.Release;
import com.xmu.project.mapper.ReleaseMapper;
import com.xmu.project.service.ReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class ReleaseImpl implements ReleaseService {
    @Autowired
    private ReleaseMapper releaseMapper;

    //查询发布
    @Override
    public List<Release> getAllRelease(Integer id) { return releaseMapper.getAllReleaseIdByUserId(id); }

    //删除发布
    @Override
    public Integer removeRelease(Release release) { return releaseMapper.deleteRelease(release); }

    //添加发布
    @Override
    public Integer addRelease(Release release) { return releaseMapper.addRelease(release); }

    //查询商品发布
    @Override
    public Release getReleaseByGoodsId(Integer id){return releaseMapper.getReleaseByGoodsId(id);}

}
