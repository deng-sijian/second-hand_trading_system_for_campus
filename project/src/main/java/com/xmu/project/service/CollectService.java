package com.xmu.project.service;

import com.xmu.project.bean.Collect;

import java.util.List;

public interface CollectService {

    //根据用户id得到所有的收藏
    List<Collect> getAllCollectById(Integer id);

    //删除收藏
    Integer removeCollect(Collect collect);

    //添加收藏
    Integer addCollect(Collect collect);

    //删除收藏
    Integer updateCollect(Collect collect);

}
