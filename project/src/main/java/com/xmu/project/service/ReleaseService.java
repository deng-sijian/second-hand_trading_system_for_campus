package com.xmu.project.service;

import com.xmu.project.bean.Release;

import java.util.List;

public interface ReleaseService {
    //删除发布
    Integer removeRelease(Release release);

    //查询发布
    List<Release> getAllRelease(Integer id);

    //添加发布
    Integer addRelease(Release release);

    //查询商品发布
    Release getReleaseByGoodsId(Integer id);
}
