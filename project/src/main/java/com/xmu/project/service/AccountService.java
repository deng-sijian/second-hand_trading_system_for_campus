package com.xmu.project.service;

import com.xmu.project.bean.User;

import java.util.List;

public interface AccountService {

    //得到所有用户
    List<User> getAllUsers();

    //用户的注册
    Integer userRegister(User user);

    //用户的登录
    User userLogin(User user);

    //用户的查询
    User queryUser(Integer id);

    //用户信息的修改
    Integer userUpdate(User newUser);

    //删除用户
    Integer removeUser(Integer id);
}
