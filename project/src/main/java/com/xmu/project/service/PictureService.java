package com.xmu.project.service;

import com.xmu.project.bean.Picture;

import java.util.List;

public interface PictureService {
    //根据商品id找到所有图片
    List<Picture> getAllPicturesByGoodsId(Integer id);
    //删除
    Integer deletePicturesByPictureId(Integer id);
    //更新
    Integer updatePictures(Picture picture);
    //添加图片
    Integer addPictures(Picture picture);
}
