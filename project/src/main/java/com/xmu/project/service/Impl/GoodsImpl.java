package com.xmu.project.service.Impl;

import com.xmu.project.bean.Goods;
import com.xmu.project.mapper.GoodsMapper;
import com.xmu.project.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GoodsImpl implements GoodsService {
    @Autowired
    GoodsMapper goodsMapper;

    @Override
    public List<Goods> getAllGoods(){return goodsMapper.getAllGoods();}
    @Override
    public Integer insertGoods(Goods goods) {
        return goodsMapper.addGoods(goods);
    }

    @Override
    public Goods queryGoods(Integer id) {
        return goodsMapper.queryGoodsById(id);
    }

    @Override
    public List<Goods> queryGoodsName(String name) {
        return goodsMapper.queryGoodsByName(name);
    }

    @Override
    public List<Goods> queryGoodsType(String type) {
        return goodsMapper.queryGoodsByKind(type);
    }

    @Override
    public Integer updateGoods(Goods newGoods) {
        return goodsMapper.updateGoods(newGoods);
    }

    @Override
    public Integer removeGoods(Goods goods) {
        goodsMapper.deleteGoods(goods.getId());
        return 1;
    }
}
