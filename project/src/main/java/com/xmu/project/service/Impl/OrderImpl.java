package com.xmu.project.service.Impl;

import com.xmu.project.bean.Order;
import com.xmu.project.mapper.OrdersMapper;
import com.xmu.project.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrderImpl implements OrderService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Override
    public Integer addOrder(Order order) {
        return ordersMapper.addOrder(order);
    }

    @Override
    public List<Order> getAllOrdersByBuyerId(Integer id) {
        return ordersMapper.getAllOrdersIdByBuyerId(id);
    }

    @Override
    public List<Order> getAllOrdersBySellerId(Integer id) {
        return ordersMapper.getAllOrdersIdBySellerId(id);
    }

    @Override
    public List<Order> getAllOrders() { return ordersMapper.getAllOrders(); }

    @Override
    public Order getOrder(Integer id) { return ordersMapper.queryOrder(id); }

    @Override
    public Integer removeOrder(Integer id) {
        return ordersMapper.deleteOrder(id);
    }
}
