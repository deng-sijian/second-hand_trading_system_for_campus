package com.xmu.project.service;

import com.xmu.project.bean.Order;

import java.util.List;

public interface OrderService {

    //添加订单
    Integer addOrder(Order order);

    //删除订单
    //Integer removeOrder();

    //根据买家id来得到所有订单
    List<Order> getAllOrdersByBuyerId(Integer id);

    //根据卖家id得到所有订单
    List<Order> getAllOrdersBySellerId(Integer id);

    //得到所有订单
    List<Order> getAllOrders();

    //通过订单id得到订单
    Order getOrder(Integer id);

    //删除订单
    Integer removeOrder(Integer id);

}
