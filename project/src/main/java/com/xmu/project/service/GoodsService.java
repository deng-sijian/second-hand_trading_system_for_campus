package com.xmu.project.service;

import com.xmu.project.bean.Goods;

import java.util.List;

public interface GoodsService {
    //得到所有商品
    List<Goods> getAllGoods();
    //插入商品
    Integer insertGoods(Goods goods);

    //查询商品详情
    Goods queryGoods(Integer id);

    //根据名字查询商品
    List<Goods> queryGoodsName(String name);

    //根据种类查询商品
    List<Goods> queryGoodsType(String type);

    //修改商品信息
    Integer updateGoods(Goods newGoods);

    //删除商品
    Integer removeGoods(Goods goods);
}
