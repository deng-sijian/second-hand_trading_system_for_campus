package com.xmu.project.service.Impl;

import com.xmu.project.bean.Picture;
import com.xmu.project.mapper.PicturesMapper;
import com.xmu.project.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PictureImpl implements PictureService {
    @Autowired
    private PicturesMapper picturesMapper;

    @Override
    public List<Picture> getAllPicturesByGoodsId(Integer id) {
        return picturesMapper.getAllPicturesByGoodsId(id);
    }

    @Override
    public Integer deletePicturesByPictureId(Integer id) {
        return picturesMapper.deletePicturesByPictureId(id);
    }

    @Override
    public Integer updatePictures(Picture picture) {
        return picturesMapper.updatePictures(picture);
    }

    @Override
    public Integer addPictures(Picture picture) {
        return picturesMapper.addPictures(picture);
    }


}
