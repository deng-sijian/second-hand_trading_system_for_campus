package com.xmu.project.service.Impl;

import com.xmu.project.bean.User;
import com.xmu.project.mapper.UsersMapper;
import com.xmu.project.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountImpl implements AccountService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public List<User> getAllUsers(){return usersMapper.getAllUsers();}

    @Override
    public Integer userRegister(User user) {
        return usersMapper.addUser(user);
    }

    @Override
    public User queryUser(Integer id){return usersMapper.queryUser(id);}

    @Override
    public User userLogin(User user) {
        User tempUser = usersMapper.queryUser(user.getId());
        if(tempUser.getPassword().equals(user.getPassword()))
            return tempUser;
        else return null;
    }

    @Override
    public Integer userUpdate(User newUser) {
        return usersMapper.updateUser(newUser);
    }

    @Override
    public Integer removeUser(Integer id) {
        return usersMapper.deleteUser(id);
    }
}
