package com.xmu.project.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringBlurMatch {

    public static boolean StringBlurMatch3(String base,String searchItem){
            int count = 0;
            Pattern p = Pattern.compile( searchItem );
            Matcher m = p.matcher(base);
            if(m.find())return true;
            else return false;
        }

        //中等模糊的查询方法，字符串需要保证大致的字符顺序
        public static boolean StringBlurMatch2(String base,String searchItem){
        int firstOccur=0;
        for(int i=0;i<searchItem.length();++i) {
            String first = searchItem.substring(i,i+1);
            firstOccur = base.substring(firstOccur).indexOf(first);
            if(firstOccur==-1)return false;
        }
        return true;
        }

        //最模糊的查询方法，只要有一个char相等就行
    public static boolean StringBlurMatch(String base,String searchItem){
        int count=0;
        for(int i=0;i<searchItem.length();++i) {
            String first = searchItem.substring(i,i+1);
            int firstOccur = base.indexOf(first);
            if(firstOccur!=-1)count++;
        }
        if(count==0)return false;
        else return true;
    }


}
