package com.xmu.project.util;

import java.security.SecureRandom;
import org.apache.commons.lang.RandomStringUtils;

public class RandomKeyGenerator {
    public static Integer generateIntKey(int bound)
    {
        return new SecureRandom().nextInt((int) Math.pow(10, bound));
    }
    public static String generateStringKey(int bound)
    {
        return RandomStringUtils.random(bound);
    }
}
