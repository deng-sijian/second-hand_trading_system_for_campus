# second-hand_trading_system_for_campus

#### 介绍

厦门大学

2019级本科第三学期Java程序设计实践课程项目

校园二手交易系统

![首页](note/shouye.png)

![登录](note/denglu.png)

![我的小店](note/wode.png)

![管理员](note/guanliyuan.png)

#### 软件架构
软件架构说明

Spring-Boot, MyBatis, Thymeleaf, Bootstrap，animate.css

数据库：mySQL

实验测试用云服务器：华为云HECS+华为云数据库RDS

测试服务器网址：[SHCTradingSys](http://123.60.53.236:8080/)(已过期）

（目前已暂停更新）

#### 使用说明

  1. project：项目原代码
  2. note：日志细节
  3. SQL：数据库相关
  4. references：参考，配置

#### 2022年更新说明

一年之后因为课程的缘故重新回顾了这个项目，在软件测试上重新对项目的一些内容进行了更改。

在一年的学习后发现大二时候做的项目在很多方面有很多不足的地方，首先是页面没有考虑到分页的需求，导致性能测试的时候经常出现压力陡增的情况。

其次是管理员界面比较拉跨的功能，缘于当初对于web请求的不了解，大量地采用了GET、刷新页面的方式来改变页面样式。

另外，支付的功能并没有使用真正的支付接口（例如支付宝、微信支付），只是简单的定义了订单和支付的关系。并设计了简单的完成订单界面。

另外附上当时主要参考的项目。

> https://gitee.com/xzlmk/Campus_shops

> https://github.com/newbee-ltd/newbee-mall

![感谢](project/src/main/resources/static/images/login/mark.png)

